// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('getTextFromSelectrorFromIndex', (selector, Index) => {
    var textScreen
    cy.get(selector).eq(Index).should($div => {
        textScreen = $div.text()
        return textScreen
    })
});

Cypress.Commands.add('dateDiff', (firstDate, secondDate) => {
    let firstDateConverted = new Date(firstDate)
    let secondDateConverted = new Date(secondDate)
    let dateDiff = firstDateConverted.getMonth() - secondDateConverted.getMonth() + (12 * (firstDateConverted.getFullYear() - secondDateConverted.getFullYear()))
    return dateDiff
});

Cypress.Commands.add('customClickElement', (element, waitTime=0) => {
    cy.get(element).click({force: true, scrollBehavior: false})
    cy.wait(waitTime)
})

Cypress.Commands.add('customClickElementMultiple', (element, timeToClick, waitTime=0) => {
    for (let index = 0; index < timeToClick; index++) {
        cy.get(element).click({force: true, scrollBehavior: false})
        cy.wait(waitTime)
    }
})
