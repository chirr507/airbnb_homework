require('cypress-xpath')

describe('User page', () => {

    beforeEach(function () {
      // "this" points at the test context object
        cy.fixture('airbnb').then((airbnbData) => {
        // "this" is still the test context object
            this.airbnbData = airbnbData
            cy.visit({
                url: this.airbnbData.url,
                failOnStatusCode: true, 
                retryOnNetworkFailure: true, 
                retryOnStatusCodeFailure: true
            })
        })
        cy.wait(2000)
    })

    it('TC001 - Search by calendar from date and to date', () => {
        cy.fixture('airbnb').then((airbnbData) => { 
            const testData = airbnbData.TC001
            const searchInput = 'input[id="bigsearch-query-detached-query"]'
            const fromDate = new Date(testData.fromDate).toLocaleDateString('en-CA')
            const toDate = new Date(testData.toDate).toLocaleDateString('en-CA')
            cy.get(searchInput).click({scrollBehavior: false})
            cy.get(searchInput).type("Bangkok", {scrollBehavior: false})
            cy.get('div[data-testid="structured-search-input-field-split-dates-0"]').click({scrollBehavior: false})

            // Select from Date
            cy.getTextFromSelectrorFromIndex('div[data-testid="structured-search-input-field-dates-panel"] div[aria-label="Calendar"] div[data-visible="true"] > div > div', 0).then(($data) => {
                const currentMonthScreenDate = $data.text()
                cy.dateDiff(currentMonthScreenDate, fromDate).then(($diffCount) => {
                    let monthDiff = $diffCount
                    let clickCount = Math.abs(monthDiff)
                    if (monthDiff < 0) {
                        cy.customClickElementMultiple('button[aria-label="Next"] svg', clickCount, 500)
                    } else if (monthDiff > 0) {
                        cy.customClickElementMultiple('button[aria-label="Next"] svg', clickCount ,500)
                    }
                })
            })
            let fromDateElement = `div[data-testid="structured-search-input-field-dates-panel"] div[aria-label="Calendar"] div[data-visible="true"] table tbody tr td div[data-testid="datepicker-day-${fromDate}"]`
            cy.customClickElement(fromDateElement, 500)

            // Select To Date
            cy.getTextFromSelectrorFromIndex('div[data-testid="structured-search-input-field-dates-panel"] div[aria-label="Calendar"] div[data-visible="true"] > div > div', 0).then(($data) => {
                const currentMonthScreenDate = $data.text()
                cy.dateDiff(currentMonthScreenDate, toDate).then(($diffCount) => {
                    let monthDiff = $diffCount
                    let clickCount = Math.abs(monthDiff)
                    if (monthDiff < 0) {
                        cy.customClickElementMultiple('button[aria-label="Next"] svg', clickCount, 500)
            
                    } else if (monthDiff > 0) {
                        cy.customClickElementMultiple('button[aria-label="Next"] svg', clickCount, 500)
                    }
                })
            })
            let toDateElement = `div[data-testid="structured-search-input-field-dates-panel"] div[aria-label="Calendar"] div[data-visible="true"] table tbody tr td div[data-testid="datepicker-day-${toDate}"]` 
            cy.customClickElement(toDateElement, 1000)
            
            // Select guest
            cy.customClickElement('div[data-testid="structured-search-input-field-guests-button"]', 100)
            cy.customClickElementMultiple('button[data-testid="stepper-adults-increase-button"]', testData.adults, 100)
            cy.customClickElementMultiple('button[data-testid="stepper-children-increase-button"]', testData.children, 100)
            cy.customClickElementMultiple('button[data-testid="stepper-infants-increase-button"]', testData.infants, 100)
            cy.customClickElement('button[data-testid="structured-search-input-search-button"]')
            cy.wait(1000)
            cy.get('#ExploreLayoutController h1').should(($el) => {
                expect($el).to.contain('Bangkok')
            })
            cy.get('div[itemprop="itemListElement"]').should(($lis) => {
                expect($lis.length).to.be.greaterThan(1)
            })
        })
    })

    it('TC002 - Search by flexible month ', () => {
        cy.fixture('airbnb').then((airbnbData) => { 
            const testData = airbnbData.TC002
            const searchInput = 'input[id="bigsearch-query-detached-query"]'
            cy.customClickElement(searchInput)
            cy.get(searchInput).type("Bangkok", {scrollBehavior: false})
            cy.customClickElement('div[data-testid="structured-search-input-field-split-dates-0"]', 500)
            cy.customClickElement('button[id="tab--tabs--1"]', 500)

            // Select stay type
            console.log("stayType: ", testData.stayType)
            if (testData.stayType == "weekend") {
                cy.customClickElementMultiple('div[id="flexible_trip_lengths-weekend_trip"] button', 2, 500)
            } else if(testData.stayType == "week"){
                cy.customClickElementMultiple('div[id="flexible_trip_lengths-one_week"] button', 2, 500)
            } else {
                cy.customClickElementMultiple('div[id="flexible_trip_lengths-one_month"] button', 2, 500)
            }

            // Select month
            testData.selectedMonths.forEach(monthName => {
                let monthElement = `div[id="flexible_trip_dates-${monthName}"] button`
                cy.get(monthElement).then(($elm) => {
                    let pressedStatus = $elm.attr('aria-pressed')
                    console.log(pressedStatus)
                    if (pressedStatus == "false") {
                        console.log("Click this month")
                        cy.customClickElement(monthElement, 500)
                    }
                });
            });

            // Unselect month
            testData.notSelectedMonths.forEach(monthName => {
                let monthElement = `div[id="flexible_trip_dates-${monthName}"] button`
                cy.get(monthElement).then(($elm) => {
                    let pressedStatus = $elm.attr('aria-pressed')
                    console.log(pressedStatus)
                    if (pressedStatus == "true") {
                        console.log("Click this month")
                        cy.customClickElement(monthElement, 500)
                    }
                });
            });

            cy.customClickElement('div[data-testid="structured-search-input-field-guests-button"]', 100)
            cy.customClickElementMultiple('button[data-testid="stepper-adults-increase-button"]', testData.adults, 100)
            cy.customClickElementMultiple('button[data-testid="stepper-children-increase-button"]', testData.children, 100)
            cy.customClickElementMultiple('button[data-testid="stepper-infants-increase-button"]', testData.infants, 100)
            cy.customClickElement('button[data-testid="structured-search-input-search-button"]')
            cy.wait(1000)
            cy.get('#ExploreLayoutController h1').should(($el) => {
                expect($el).to.contain('Bangkok')
            })
            cy.get('div[itemprop="itemListElement"]').should(($lis) => {
                expect($lis.length).to.be.greaterThan(1)
            })
        })
    })
})